package com.example.inwestinfra.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.inwestinfra.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class SortByFragment extends BottomSheetDialogFragment {
    public static final String TAG = "bottom_sheet";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.sortby_fragment, container, false);
        return v;
    }
}
