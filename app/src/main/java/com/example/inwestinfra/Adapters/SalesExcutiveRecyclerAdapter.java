package com.example.inwestinfra.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.inwestinfra.Activities.ManagersListActivity;
import com.example.inwestinfra.Activities.SAlesExcutiveListActivity;
import com.example.inwestinfra.Models.ManagersModel;
import com.example.inwestinfra.R;

public class SalesExcutiveRecyclerAdapter extends RecyclerView.Adapter<SalesExcutiveRecyclerAdapter.Holder> {
    ManagersModel[] managersModels;
    Context context;
    public SalesExcutiveRecyclerAdapter(ManagersModel[] data, SAlesExcutiveListActivity managersListActivity) {
        this.managersModels=data;
        this.context=managersListActivity;
    }

    @NonNull
    @Override
    public SalesExcutiveRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sales, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SalesExcutiveRecyclerAdapter.Holder holder, int position) {

        holder.txt_name.setText(managersModels[position].getName());
        holder.txt_mno.setText(managersModels[position].getMobile());
        holder.txt_mail.setText(managersModels[position].getMail());

    }

    @Override
    public int getItemCount() {
        return managersModels.length;
    }
    class Holder extends RecyclerView.ViewHolder{

        TextView txt_name,txt_mno,txt_mail;

        public Holder(@NonNull View itemView) {
            super(itemView);
            txt_mail=itemView.findViewById(R.id.txt_mail);
            txt_mno=itemView.findViewById(R.id.txt_mno);
            txt_name=itemView.findViewById(R.id.txt_name);

        }
    }
}
