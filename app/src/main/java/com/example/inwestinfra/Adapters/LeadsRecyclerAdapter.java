package com.example.inwestinfra.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.inwestinfra.Activities.LeadListActivity;
import com.example.inwestinfra.Activities.SAlesExcutiveListActivity;
import com.example.inwestinfra.Models.LeadsModel;
import com.example.inwestinfra.Models.ManagersModel;
import com.example.inwestinfra.R;

public class LeadsRecyclerAdapter extends RecyclerView.Adapter<LeadsRecyclerAdapter.Holder> {
    LeadsModel[] managersModels;
    Context context;
    public LeadsRecyclerAdapter(LeadsModel[] data, LeadListActivity managersListActivity) {
        this.managersModels=data;
        this.context=managersListActivity;
    }

    @NonNull
    @Override
    public LeadsRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_leads, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LeadsRecyclerAdapter.Holder holder, int position) {

        holder.txt_lname.setText(managersModels[position].getL_name());
        holder.txt_mno.setText(managersModels[position].getMno());
        holder.txt_pname.setText(managersModels[position].getP_name());

    }

    @Override
    public int getItemCount() {
        return managersModels.length;
    }
    class Holder extends RecyclerView.ViewHolder{

        TextView txt_lname,txt_mno,txt_pname;

        public Holder(@NonNull View itemView) {
            super(itemView);
            txt_lname=itemView.findViewById(R.id.txt_lname);
            txt_mno=itemView.findViewById(R.id.txt_mno);
            txt_pname=itemView.findViewById(R.id.txt_pname);

        }
    }
}
