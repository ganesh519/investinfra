package com.example.inwestinfra.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.inwestinfra.Activities.LeadListActivity;
import com.example.inwestinfra.Activities.ManagersListActivity;
import com.example.inwestinfra.Activities.SAlesExcutiveListActivity;
import com.example.inwestinfra.CrmListItemClickListener;
import com.example.inwestinfra.Models.HomeCategoryModel;
import com.example.inwestinfra.R;


public class CategoriesRecyclerAdapter extends RecyclerView.Adapter<CategoriesRecyclerAdapter.Holder> {
    private HomeCategoryModel[] categoryData;
    Context context;

    public CategoriesRecyclerAdapter(HomeCategoryModel[] data, Context homeFragment) {
        this.categoryData = data;
        this.context = homeFragment;

    }

    @NonNull
    @Override
    public CategoriesRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home_category, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesRecyclerAdapter.Holder holder, int position) {

        holder.category_title.setText(categoryData[position].getCateg_name());
        holder.txt_count.setText(categoryData[position].getTxt_count());

        holder.setItemClickListener(new CrmListItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {

                if(pos==0)
                {
                    context.startActivity(new Intent(context.getApplicationContext(), ManagersListActivity.class));
                }
                else if(pos==1)
                {
                    context.startActivity(new Intent(context.getApplicationContext(), SAlesExcutiveListActivity.class));
                }
                else if(pos==2)
                {
                    context.startActivity(new Intent(context.getApplicationContext(), LeadListActivity.class));
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return categoryData.length;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView img_categroy;
        public CardView frame_week;
        public TextView category_title,txt_count;
        CrmListItemClickListener crmListItemClickListener;

        public Holder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            category_title = itemView.findViewById(R.id.category_title);
            txt_count = itemView.findViewById(R.id.txt_count);

        }

        @Override
        public void onClick(View view) {
            this.crmListItemClickListener.onItemClick(view, getLayoutPosition());
        }
        public void setItemClickListener(CrmListItemClickListener ic) {
            this.crmListItemClickListener = ic;
        }

    }
}
