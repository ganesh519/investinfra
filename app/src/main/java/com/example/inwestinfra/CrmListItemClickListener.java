package com.example.inwestinfra;

import android.view.View;

public interface CrmListItemClickListener
{
    void onItemClick(View v, int pos);
}
