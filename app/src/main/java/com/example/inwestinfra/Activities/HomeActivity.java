package com.example.inwestinfra.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.inwestinfra.Adapters.CategoriesRecyclerAdapter;
import com.example.inwestinfra.Fragments.SortByFragment;
import com.example.inwestinfra.Models.HomeCategoryModel;
import com.example.inwestinfra.R;

public class HomeActivity extends AppCompatActivity {
    RecyclerView recyclerViewCategory;
    CategoriesRecyclerAdapter categoriesRecyclerAdapter;
    RelativeLayout relative_dashboard;
    CardView cardview;
    public static final String TAG = "bottom_sheet";
    public static SortByFragment sortByFragment;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        cardview=findViewById(R.id.cardview);
        cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sortByFragment = new SortByFragment();
                sortByFragment.show(getSupportFragmentManager(), TAG);


            }
        });

        relative_dashboard=findViewById(R.id.relative_dashboard);
        relative_dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this,DashboardActivity.class));
            }
        });

        recyclerViewCategory=findViewById(R.id.recyclerViewCategory);

        HomeCategoryModel[] data = new HomeCategoryModel[] {
                new HomeCategoryModel("MANAGERS", "9"),
                new HomeCategoryModel("SALES EXCUTIVES", "99"),
                new HomeCategoryModel("LEADS","999"),
                new HomeCategoryModel("REPORTS", "9999"),
        };
        categoriesRecyclerAdapter = new CategoriesRecyclerAdapter(data, HomeActivity.this);
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(HomeActivity.this, 2);
        recyclerViewCategory.setNestedScrollingEnabled(false);
        recyclerViewCategory.setLayoutManager(gridLayoutManager);
        recyclerViewCategory.setAdapter(categoriesRecyclerAdapter);
    }
}
