package com.example.inwestinfra.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.inwestinfra.Adapters.ManagersRecyclerAdapter;
import com.example.inwestinfra.Adapters.SalesExcutiveRecyclerAdapter;
import com.example.inwestinfra.Models.ManagersModel;
import com.example.inwestinfra.R;

public class SAlesExcutiveListActivity extends AppCompatActivity {
    RecyclerView recycler_sales;
    SalesExcutiveRecyclerAdapter salesExcutiveRecyclerAdapter;
    RelativeLayout relative_add;
    ImageView back;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.salesexcutive_activity);

        back=findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        relative_add=findViewById(R.id.relative_add);
        relative_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SAlesExcutiveListActivity.this,AddSaleExcutiveActivity.class));
            }
        });


        recycler_sales=findViewById(R.id.recycler_sales);


        ManagersModel[] data = new ManagersModel[] {
                new ManagersModel("Raju", "1234567890","raju123@gmail.com"),
                new ManagersModel("Raju", "1234567890","raju123@gmail.com"),
                new ManagersModel("Raju", "1234567890","raju123@gmail.com"),
                new ManagersModel("Raju", "1234567890","raju123@gmail.com"),
                new ManagersModel("Raju", "1234567890","raju123@gmail.com"),
                new ManagersModel("Raju", "1234567890","raju123@gmail.com")
        };

        salesExcutiveRecyclerAdapter=new SalesExcutiveRecyclerAdapter(data,SAlesExcutiveListActivity.this);
        final LinearLayoutManager layoutManager1 = new LinearLayoutManager(SAlesExcutiveListActivity.this);
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recycler_sales.setLayoutManager(layoutManager1);
        recycler_sales.setNestedScrollingEnabled(false);
        recycler_sales.setAdapter(salesExcutiveRecyclerAdapter);


    }
}
