package com.example.inwestinfra.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.inwestinfra.Adapters.LeadsRecyclerAdapter;
import com.example.inwestinfra.Adapters.ManagersRecyclerAdapter;
import com.example.inwestinfra.Models.LeadsModel;
import com.example.inwestinfra.Models.ManagersModel;
import com.example.inwestinfra.R;

public class LeadListActivity extends AppCompatActivity {
    RecyclerView recycler_leads;
    LeadsRecyclerAdapter leadsRecyclerAdapter;
    RelativeLayout relative_add;
    ImageView back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leadslist_activity);

        back=findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        relative_add=findViewById(R.id.relative_add);
        relative_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LeadListActivity.this,AddLeadActivity.class));
            }
        });

        recycler_leads=findViewById(R.id.recycler_leads);

        LeadsModel[] data = new LeadsModel[] {
                new LeadsModel("Raju", "PROJECT NAME","1234567890"),
                new LeadsModel("Raju", "PROJECT NAME","1234567890"),
                new LeadsModel("Raju", "PROJECT NAME","1234567890"),
                new LeadsModel("Raju", "PROJECT NAME","1234567890"),
                new LeadsModel("Raju", "PROJECT NAME","1234567890"),
                new LeadsModel("Raju", "PROJECT NAME","1234567890")
        };

        leadsRecyclerAdapter=new LeadsRecyclerAdapter(data,LeadListActivity.this);
        final LinearLayoutManager layoutManager1 = new LinearLayoutManager(LeadListActivity.this);
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recycler_leads.setLayoutManager(layoutManager1);
        recycler_leads.setNestedScrollingEnabled(false);
        recycler_leads.setAdapter(leadsRecyclerAdapter);
    }
}
