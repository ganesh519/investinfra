package com.example.inwestinfra.Models;

public class ManagersModel {

    String name,mobile,mail;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public ManagersModel(String name, String mobile, String mail) {
        this.name = name;
        this.mobile = mobile;
        this.mail = mail;
    }
}
