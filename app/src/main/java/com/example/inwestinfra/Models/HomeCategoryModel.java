package com.example.inwestinfra.Models;

public class HomeCategoryModel
{
   //private String cate_id;
    private String categ_name,txt_count;

    public String getCateg_name() {
        return categ_name;
    }

    public void setCateg_name(String categ_name) {
        this.categ_name = categ_name;
    }

    public String getTxt_count() {
        return txt_count;
    }

    public void setTxt_count(String txt_count) {
        this.txt_count = txt_count;
    }

    public HomeCategoryModel(String categ_name, String txt_count) {
        this.categ_name = categ_name;
        this.txt_count = txt_count;
    }
}
