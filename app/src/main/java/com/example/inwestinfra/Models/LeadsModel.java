package com.example.inwestinfra.Models;

public class LeadsModel {

    String l_name,p_name,mno;

    public String getL_name() {
        return l_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getMno() {
        return mno;
    }

    public void setMno(String mno) {
        this.mno = mno;
    }

    public LeadsModel(String l_name, String p_name, String mno) {
        this.l_name = l_name;
        this.p_name = p_name;
        this.mno = mno;
    }
}
